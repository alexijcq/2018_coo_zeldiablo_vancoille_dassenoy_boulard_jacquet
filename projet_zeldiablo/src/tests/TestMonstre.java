package tests;
import static org.junit.Assert.*;

import org.junit.Test;

import principale.Aventurier;
import principale.Cases;
import principale.Monstre;
import principale.Personnage;


public class TestMonstre {

	@Test
	public void testConstructeur() {
		Monstre m = new Monstre(5,10);
		assertEquals("le Monstre devrait se situer en abscisse 5", 5, m.getX());
		assertEquals("le Monstre devrait se situer en ordonnee 10", 10, m.getY());
		assertEquals("le Monstre devrait avoir 3 pdv", 3, m.getPdv());
	}
	
	@Test
	public void testPlacerMonstre() {
		Monstre m =new Monstre(0,0);
		Cases cas=new Cases(5,10);
		m.placerPersonnage(cas);
		assertEquals("le Monstre devrait se situer en abscisse 5", 5, m.getX());
		assertEquals("le Monstre devrait se situer en ordonnee 10", 10, m.getY());
	}
	@Test
	public void testDeplacement() {
		Personnage m = new Monstre(5,10);
		String str = ((Monstre) m).genererDirection();
		m.deplacer(str);
		boolean change=false;
		if(m.getX()!=5 || m.getY()!=10)
			change=true;
		assertEquals("le boolean change devrait etre a true", true, change);
	
	}
	@Test
	public void testAttaquer() {
		Monstre m =new Monstre(6,5);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		a.subirDegat(m.attaquer(a.getCases()));
		assertEquals("l'aventurier devrait avoir 4 pdv", 4, a.getPdv());
		
	}
	@Test
	public void testAttaquer_2() {
		Monstre m =new Monstre(4,5);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		a.subirDegat(m.attaquer(a.getCases()));
		assertEquals("l'aventurier devrait avoir 4 pdv", 4, a.getPdv());
		
	}
	@Test
	public void testAttaquer_3() {
		Monstre m =new Monstre(5,6);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		a.subirDegat(m.attaquer(a.getCases()));
		assertEquals("l'aventurier devrait avoir 4 pdv", 4, a.getPdv());
		
	}
	@Test
	public void testAttaquer_4() {
		Monstre m =new Monstre(5,4);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		a.subirDegat(m.attaquer(a.getCases()));
		assertEquals("l'aventurier devrait avoir 4 pdv", 4, a.getPdv());
		
	}
	
	
	@Test
	public void testAttaquer_5() {
		Monstre m =new Monstre(8,8);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		a.subirDegat(m.attaquer(a.getCases()));
		assertEquals("l'aventurier devrait avoir 5 pdv", 5, a.getPdv());
	
	}
	
}

package tests;


import static org.junit.Assert.*;

import org.junit.Test;

import principale.Amulette;
import principale.CaseObjet;
import principale.CasePiege;
import principale.Cases;
import principale.Jeu;
import principale.Monstre;


public class TestJeu {

	@Test
	public void test_deplacerAventurier() {
		Jeu j = new Jeu();

		assertEquals("devrait etre a 1",1,j.getAventurier().getX());
		assertEquals("devrait etre a 4",4,j.getAventurier().getY());

		j.actionPersonnage("z");

		assertEquals("devrait etre a 1",1,j.getAventurier().getX());
		assertEquals("devrait etre a 3",3,j.getAventurier().getY());
	}

	@Test
	public void test_collisionObstacle_Bas_Aventurier(){
		Jeu j = new Jeu();

		assertEquals("position de l'aventurier en Y=4",4,j.getAventurier().getY());
		assertEquals("devrait entrer en collision en Y=4",true,j.collisionObstacle("s",j.getAventurier()));
	}

	@Test
	public void test_collisionObstacle_Haut_Aventurier(){
		Jeu j = new Jeu();

		for(int i = 0 ; i < 4 ; i++){
			j.actionPersonnage("z");
		}

		assertEquals("position de l'aventurier en Y=1",1,j.getAventurier().getY());
		assertEquals("devrait entrer en collision en Y=1",true,j.collisionObstacle("z",j.getAventurier()));
	}

	@Test
	public void test_collisionObstacle_Gauche_Aventurier(){
		Jeu j = new Jeu();

		j.actionPersonnage("z");
		for(int i = 0 ; i <4 ; i++){
			j.actionPersonnage("q");
		}

		assertEquals("position de l'aventurier en X=1",1,j.getAventurier().getX());
		assertEquals("devrait entrer en collision en X=1",true,j.collisionObstacle("q",j.getAventurier()));
	}

	@Test
	public void test_collisionObstacle_Droite_Aventurier(){
		Jeu j = new Jeu();

		assertEquals("position de l'aventurier en X=1",1,j.getAventurier().getX());
		assertEquals("devrait entrer en collision en X=8",true,j.collisionObstacle("d",j.getAventurier()));
	}

	@Test
	public void test_collisionObstacle_Monstre(){
		Jeu j = new Jeu();
		Monstre m = new Monstre(7,13);
		j.getListeMob().add(m);

		assertEquals("position du monstre en Y=13",13,j.getListeMob().get(1).getY());
		assertEquals("devrait entrer en collision en Y=13",true,j.collisionObstacle("s",j.getListeMob().get(1)));
	}

	@Test
	public void test_majLab(){
		Jeu j = new Jeu();
		j.actionPersonnage("z");
		Cases c = new Cases(1,2);

		assertEquals("devrait etre a 1",1,j.getAventurier().getX());
		assertEquals("devrait etre a 3",3,j.getAventurier().getY());

		j.getListeMob().get(0).placerPersonnage(c);

		assertEquals("devrait etre a 1",1,j.getListeMob().get(0).getX());
		assertEquals("devrait etre a 2",2,j.getListeMob().get(0).getY());

		j.getLabyrinthe().majLab(j.getListeMob().get(0).getX(),j.getListeMob().get(0).getY());
		assertEquals("devrait entrer en collision en Y=7",true,j.collisionObstacle("z",j.getAventurier()));

	}

	@Test
	public void test_actionPersonnage_e(){
		Jeu j = new Jeu();

		j.getLabyrinthe().getTabcase()[5][5] = new CaseObjet(5,5,new Amulette());
		j.actionPersonnage("e");

		assertEquals("devrait avoir une amulette dans inventaire",1,j.getAventurier().getInventaire().size());
	}

	@Test
	public void test_verifPiege(){
		Jeu j= new Jeu();
		j.getLabyrinthe().getTabcase()[1][5]= new CasePiege(1,5);
		boolean hit=false;
		int pdv1;
		int pdv2;
		pdv1=j.getAventurier().getPdv();
		j.actionPersonnage("z");
		pdv2=j.getAventurier().getPdv();
		if(pdv1!=pdv2) {
			hit=true;
		}		
	}
	
	@Test
	public void test_aventurierSurSortie() {
		Jeu j = new Jeu();
		j.getAventurier().placerPersonnage(j.getLabyrinthe().getSortie());
		j.aventurierSurSortie();
		assertEquals("le deuxieme niveau devrait etre generer",2,j.getLabyrinthe().getNiveau());
	}
}

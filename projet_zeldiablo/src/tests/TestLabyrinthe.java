package tests;
import static org.junit.Assert.*;

import org.junit.Test;

import principale.Amulette;
import principale.CaseObjet;
import principale.Jeu;
import principale.Labyrinthe;


public class TestLabyrinthe {

	@Test
	public void testConstructeur() {
		Labyrinthe l = new Labyrinthe(1);

		assertEquals("La taille du labyrinthe devrait etre de 15",15,l.getTaille());
	}

	/*
	@Test
	public void testAddCases(){
		Labyrinthe l = new Labyrinthe();

		l.addCases();

		assertEquals("Le tableau devrait etre rempli de cases",l.getTabcase().length,10);
	}
	 */
	
	@Test
	public void test_etreCaseSortie() {
		Labyrinthe l = new Labyrinthe(1);
		
		assertEquals("devrait etre la case de sortie",true,l.etreCaseSortie(12,14));
	}
}

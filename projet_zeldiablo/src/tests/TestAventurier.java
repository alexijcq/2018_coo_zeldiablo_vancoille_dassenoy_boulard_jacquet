package tests;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

import principale.Aventurier;
import principale.Cases;
import principale.Monstre;


public class TestAventurier {

	@Test
	public void testConstructeur() {
		Aventurier a = new Aventurier("toto");
		assertEquals("L'aventurier devrait se situer en abscisse 1", 1, a.getX());
		assertEquals("L'aventurier devrait se situer en ordonnee 4", 4, a.getY());
		assertEquals("L'aventurier devrait se nommer toto", "toto", a.getNom());
	}
	
	@Test
	public void testChangerPositionNord() {
		Aventurier a = new Aventurier("toto");
		a.deplacer("z");
		assertEquals("L'aventurier devrait se situer en abscisse 1", 1, a.getX());
		assertEquals("L'aventurier devrait se situer en ordonnee 3", 3, a.getY());
	}
	
	@Test
	public void testChangerPositionSud() {
		Aventurier a = new Aventurier("toto");
		a.deplacer("s");
		assertEquals("L'aventurier devrait se situer en abscisse 1", 1, a.getX());
		assertEquals("L'aventurier devrait se situer en ordonnee 5", 5, a.getY());
	}
	
	@Test
	public void testChangerPositionOuest() {
		Aventurier a = new Aventurier("toto");
		a.deplacer("q");
		assertEquals("L'aventurier devrait se situer en abscisse 0", 0, a.getX());
		assertEquals("L'aventurier devrait se situer en ordonnee 4", 4, a.getY());
	}
	
	@Test
	public void testChangerPositionEst() {
		Aventurier a = new Aventurier("toto");
		a.deplacer("d");
		assertEquals("L'aventurier devrait se situer en abscisse 2", 2, a.getX());
		assertEquals("L'aventurier devrait se situer en ordonnee 4", 4, a.getY());
	}
	@Test
	public void testattaquer() {
		Monstre m =new Monstre(6,5);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		m.subirDegat(a.attaquer(m.getCases()));
		assertEquals("le Monstre devrait avoir 2 pdv", 2, m.getPdv());
		
	}
	@Test
	public void testattaquer_2() {
		Monstre m =new Monstre(4,5);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		m.subirDegat(a.attaquer(m.getCases()));
		assertEquals("le Monstre devrait avoir 2 pdv", 2, m.getPdv());
		
	}
	@Test
	public void testattaquer_3() {
		Monstre m =new Monstre(5,6);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		m.subirDegat(a.attaquer(m.getCases()));
		assertEquals("le Monstre devrait avoir 2 pdv", 2, m.getPdv());
		
	}
	@Test
	public void testattaquer_4() {
		Monstre m =new Monstre(5,4);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		m.subirDegat(a.attaquer(m.getCases()));
		assertEquals("le Monstre devrait avoir 2 pdv", 2, m.getPdv());
		
	}
	@Test
	public void testattaquer_5() {
		Monstre m =new Monstre(8,8);
		Aventurier a = new Aventurier("Johnny");
		Cases c=new Cases(5,5);
		a.placerPersonnage(c);
		m.subirDegat(a.attaquer(m.getCases()));
		assertEquals("le Monstre devrait avoir 3 pdv", 3, m.getPdv());
		
	}

}

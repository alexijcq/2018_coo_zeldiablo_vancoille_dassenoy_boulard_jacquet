package principale;
import java.util.ArrayList;


public class Labyrinthe {

	private int taille;
	private Cases[][] tabcase;
	private CaseSortie sortie;
	private int niveau;




	public Labyrinthe(int a){
		niveau=a;
		if(a==1){
			this.taille = 15;
			tabcase = new Cases[taille][taille];
			for(int i = 0 ; i < tabcase.length ; i++){
				for(int j = 0 ; j < tabcase.length ; j++){
					this.tabcase[i][j]=new CaseVide(i,j);
				}

				this.murAutour();
				// A CHANGER
				for(int b = 2 ; b < 5 ; b++){
					this.tabcase[b][2] = new CaseMur(b,2);
				}


				//carre du milieu
				for(int b = 5 ; b < 10 ; b++){
					this.tabcase[b][5] = new CaseMur(b,5);
					this.tabcase[7][5] = new CaseVide(7,5);
				}
				for(int b = 6 ; b < 10 ; b++){
					this.tabcase[5][b] = new CaseMur(5,b);
					this.tabcase[5][7] = new CaseVide(5,7);
				}
				for(int b = 5 ; b < 10 ; b++){
					this.tabcase[b][9] = new CaseMur(b,9);
					this.tabcase[7][9] = new CaseVide(7,9);
				}
				for(int b = 6 ; b < 10 ; b++){
					this.tabcase[9][b] = new CaseMur(9,b);
					this.tabcase[9][7] = new CaseVide(9,7);
				}
				//
				//diagonale
				this.tabcase[6][8] = new CaseMur(6,8);
				this.tabcase[7][7] = new CaseMur(7,7);
				this.tabcase[8][6] = new CaseMur(8,6);
				//

				for(int b = 4 ; b < 7 ; b++){
					this.tabcase[b][3] = new CaseMur(b,3);
				}
				for(int b = 1 ; b < 3 ; b++){
					this.tabcase[b][5] = new CaseMur(b,5);
				}
				for(int b = 2 ; b < 5 ; b++){
					this.tabcase[2][b] = new CaseMur(2,b);
				}
				this.tabcase[4][5] = new CaseMur(4,5);

				for(int b = 6 ; b < 12 ; b++){
					this.tabcase[2][b] = new CaseMur(2,b);
				}
				for(int b = 2 ; b < 6 ; b++){
					this.tabcase[b][10] = new CaseMur(b,10);
				}
				for(int b = 4 ; b < 8 ; b++){
					this.tabcase[b][12] = new CaseMur(b,12);
				}
				for(int b = 10 ; b < 14 ; b++){
					this.tabcase[9][b] = new CaseMur(9,b);
				}

				this.tabcase[10][8] = new CaseMur(10,8);

				for(int b = 3 ; b < 9 ; b++){
					this.tabcase[11][b] = new CaseMur(11,b);
				}
				for(int b = 0 ; b < 5 ; b++){
					this.tabcase[8][b] = new CaseMur(8,b);
				}
				for(int b = 10 ; b < 13; b++){
					this.tabcase[b][10] = new CaseMur(b,10);
				}
				for(int b = 10 ; b < 12; b++){
					this.tabcase[12][b] = new CaseMur(12,b);
				}
				for(int b = 12 ; b < 14; b++){
					this.tabcase[10][b] = new CaseMur(10,b);
				}

				this.tabcase[10][10] = new CasePiege(10,10);
				this.tabcase[2][8] = new CaseVide(2,8);
				this.tabcase[2][11] = new CaseMur(2,11);
				this.tabcase[2][13] = new CaseMur(2,13);
				this.tabcase[10][3] = new CaseMur(10,3);
				this.tabcase[10][2] = new CaseMur(10,2);

				//droite
				this.tabcase[12][1] = new CaseMur(12,1);
				this.tabcase[12][3] = new CaseMur(12,3);
				this.tabcase[13][5] = new CaseMur(13,5);
				this.tabcase[12][7] = new CaseMur(12,7);
				this.tabcase[13][9] = new CaseMur(13,9);
				this.tabcase[12][3] = new CaseMur(12,3);
				//
				this.tabcase[1][6] = new CaseObjet(1,6, new Amulette());
				this.tabcase[4][8] = new CaseMur(4,8);
				this.tabcase[4][9] = new CaseObjet(4,9, new Amulette());
				this.tabcase[13][10] = new CaseObjet(13,10, new Amulette());
				this.sortie = new CaseSortie(13,13);
				this.tabcase[13][13] = sortie;
			}

		}
	}

	public void murAutour(){
		for(int a = 0 ; a < tabcase.length ; a++){
			for(int b = 0 ; b < tabcase.length ; b++){
				this.tabcase[0][b] = new CaseMur(0,b);
				this.tabcase[a][0] = new CaseMur(a,0);
				this.tabcase[tabcase.length-1][b] = new CaseMur(tabcase.length-1,b);
				this.tabcase[a][tabcase.length-1] = new CaseMur(a,tabcase.length-1);
			}
		}
	}

	public CaseSortie getSortie() {
		return sortie;
	}

	public void setTabcase(Cases[][] tabcase) {
		this.tabcase = tabcase;
	}

	public int getTaille(){
		return this.taille;
	}

	public Cases[][] getTabcase() {
		return tabcase;
	}

	public int getNiveau() {
		return niveau;
	}


	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}

	public boolean enCollision(int x, int y) {
		return this.tabcase[x][y].estObstacle();
	}

	public void majLab(int dx,int dy){
		this.tabcase[dx][dy]=new CaseMur(dx,dy);
	}

	public Objet objetSurCase(int dx, int dy){
		Objet o = null;
		boolean trouve=false;
		if(this.tabcase[dx][dy] instanceof CaseObjet){
			trouve = this.tabcase[dx][dy].isContient();
		}
		if(trouve==true){
			o = ((CaseObjet) this.tabcase[dx][dy]).getObjet();
			
		}
		if(o !=null){
			this.tabcase[dx][dy] = new CaseVide(dx,dy);
		}
		return o;

	}

	public boolean etreCaseSortie(int x, int y) {
		// TODO Auto-generated method stub
		boolean res = false;
		int sortiex = sortie.getX();
		int sortiey = sortie.getY();
		if (x == sortiex && y == sortiey) {
			res = true;
		}
		return res;		
	}
}

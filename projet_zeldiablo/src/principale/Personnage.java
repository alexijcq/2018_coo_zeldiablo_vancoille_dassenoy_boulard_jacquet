package principale;

import java.awt.image.BufferedImage;

public abstract class Personnage {
	protected BufferedImage img;
	protected BufferedImage imghit;
	protected boolean hit;
	protected boolean att=false;
	protected Cases ca;
	protected int pdv;
	

	public Personnage() {
		
	}
	public BufferedImage getImage() {
		return this.img;
	}
	public BufferedImage getImageHit() {
		return this.imghit;
	}
	public boolean getAtt() {
		return this.att;
	}
	public void setAtt() {
		this.att=false;
	}
	public void placerPersonnage(Cases c) {
		this.ca=c;
	}

	public void changerPosition(int dx,int dy) {	
		if (((this.ca.getX() + dx) > 14) || ((this.ca.getX() + dx)< 0)){
			this.ca.setX(this.ca.getX());
		}else{
			this.ca.setX(this.ca.getX()+dx);		}

		if (((this.ca.getY() + dy) > 14) || ((this.ca.getY() + dy)< 0)){
			this.ca.setY(this.ca.getY());
		}else{
			this.ca.setY(this.ca.getY()+dy);
		}
	}
	
	public void deplacer(String s) {
		switch(s) {
		case "z":
			changerPosition(0,-1);
			break;
		case "s":
			changerPosition(0,1);
			break;
		case "q":
			changerPosition(-1,0);
			break;
		case "d":
			changerPosition(1,0);
			break;
		default:
			break;
		}
	}
	
	public int getPdv() {
		return pdv;
	}

	public void setPdv(int pdv) {
		this.pdv = pdv;
	}
	public void subirDegat(int d) {
		this.pdv=this.pdv-d;
		if(d!=0) {
			this.att = false;
			this.hit=true;
		}
	}
	
	
	public void initHit() {
		this.hit=false;
	}
	public boolean getHit() {
		return this.hit;
	}
	public void setHit() {
		this.hit=true;
	}
	
	public int attaquer(Cases c){
		if(this.ca.aCote(c)) {
			this.att=true;
			return 1;
			
		}else {
			return 0;
		}
	}
	public int getX() {
		return this.ca.getX();
	}

	public int getY() {
		return this.ca.getY();
	}
	
	public Cases getCases() {
		return this.ca;
	}
	public boolean estMort() {
		if(this.pdv==0) {
			return true;
		}else {
			return false;
		}
	}
	public void actAtt() {
		this.att=true;
	}
	public boolean collision(int dx,int dy) {
		boolean coll=false;
		if(this.ca.getX()==dx && this.ca.getY()==dy) {
			coll=true;
		}
		return coll;
	}

}

package principale;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class Aventurier extends Personnage{
	private BufferedImage imgatt;
	private String nom;
	private ArrayList<Objet> inventaire;

	public Aventurier (String n) {
		this.nom = n;
		this.ca=new Cases(1,4);
		this.pdv = 5;
		this.inventaire = new ArrayList<Objet>();
		if(img==null) {
			try {
				img = ImageIO.read(new File("src\\image\\johnny.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			try {
				imghit = ImageIO.read(new File("src\\image\\johnnyhit.png"));
				imghit=imghit.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			try {
				imgatt = ImageIO.read(new File("src\\image\\johnnypunch.png"));
				imgatt=imgatt.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		
	}
	}

	public BufferedImage getImageAtt() {
		return this.imgatt;
	}
	public String getNom() {
		return nom;
	}

	public void deplacer(String commande) {
		switch(commande) {
		case "z":
			changerPosition(0,-1);
			break;
		case "s":
			changerPosition(0,1);
			break;
		case "q":
			changerPosition(-1,0);
			break;
		case "d":
			changerPosition(1,0);
			break;
		default:
			break;
		}
	}
	
	public ArrayList<Objet> getInventaire() {
		return inventaire;
	}

	public void ramasserObjet(Objet o){
		if (ca instanceof CaseObjet) {
			this.inventaire.add(o);
		}
	}

}


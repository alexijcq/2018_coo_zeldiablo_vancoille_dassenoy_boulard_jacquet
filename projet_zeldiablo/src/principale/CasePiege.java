package principale;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CasePiege extends Cases {

	private int degat;
	
	public CasePiege(int x,int y) {
		super(x,y);
		this.degat=1;
		if(img==null) {
			try {
				img = ImageIO.read(new File("src\\image\\case.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		
	}
	}
	public int getDegat() {
		return this.degat;
	}
}

package principale;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CaseVide extends Cases{

	public CaseVide(int dx, int dy) {
		super(dx, dy);
		if(img==null) {
			try {
				img = ImageIO.read(new File("src\\image\\case.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		
	}
	}

	@Override
	public boolean estObstacle() {
		return false;
	}
	
	

}

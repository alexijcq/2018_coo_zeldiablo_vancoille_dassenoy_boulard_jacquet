package principale;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CaseMur extends Cases{

	public CaseMur(int dx, int dy) {
		super(dx, dy);
		this.obstacle = true;
		if(img==null) {
			try {
				img = ImageIO.read(new File("src\\image\\mur.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
	}
	
	}
	
	

}

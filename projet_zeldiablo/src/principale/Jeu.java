package principale;
import java.util.ArrayList;

import moteurJeu.Commande;

public class Jeu implements moteurJeu.Jeu {

	private Labyrinthe l;
	private Aventurier a;
	private ArrayList<Monstre> listeMob;
	private boolean fini;


	public Jeu () {
		this.l = new Labyrinthe(1);
		this.a = new Aventurier("le chevalier blanc");
		this.listeMob = new ArrayList<Monstre>();
		this.listeMob.add(new Monstre(6,7));
		this.fini = false;

	}

	public void actionPersonnage(String st) {
		for(int i=0;i<this.listeMob.size();i++) {
			this.listeMob.get(i).initHit();
		this.a.setAtt();
		this.a.initHit();
		}
		switch(st) {
		case "a":
			this.a.actAtt();
			for(int i=0;i<this.listeMob.size();i++) {
				this.listeMob.get(i).subirDegat(this.a.attaquer(this.listeMob.get(i).getCases()));

			}
			break;
		case "e":
			Objet o = l.objetSurCase(a.getX(), a.getY());
			if(o!=null){
				a.ramasserObjet(o);
			}
		
			l.getTabcase()[a.getX()][a.getY()] = new CaseVide(a.getX(),a.getY());
			break;
		default:

			if(collisionObstacle(st,this.a)==false && collisionPersonnage(st)==false){
				a.deplacer(st);
				//this.aventurierSurSortie();
				
				//break;
			}
			break;
			
		}
		
		for(int i=0;i<this.listeMob.size();i++) {
			String dir = this.listeMob.get(i).genererDirection();
			if(collisionObstacle(dir,this.listeMob.get(i))==false){
				this.listeMob.get(i).deplacer(dir);
				this.a.subirDegat(this.listeMob.get(i).attaquer(this.a.getCases()));
				
			}
		}
		
		System.out.println(this.a.pdv);
		for (Monstre m : listeMob) {
			System.out.println(m.pdv);
		}
		for (Objet o : a.getInventaire()) {
			System.out.println(o);
		}
		
		if (this.a.getPdv() == 0) {
			fini = true;
		}
				
		
	}
	


	public boolean collisionObstacle(String st,Personnage p) {
		int x = p.getX();
		int y = p.getY();
		boolean tmp = false;
		if(st.equals("z")){
			tmp = l.enCollision(x,y-1);
		}else if(st.equals("s")){
			tmp = l.enCollision(x,y+1);
		}else if(st.equals("q")){
			tmp = l.enCollision(x-1,y);
		}else if(st.equals("d")){
			tmp = l.enCollision(x+1,y);
		}
		return tmp;
	}
	public boolean collisionPersonnage(String st) {
		boolean tmp = false;
		for(int i=0;i<this.listeMob.size();i++) {
		if(st.equals("z")){
			tmp = this.listeMob.get(i).collision(a.getX(),a.getY()-1);
		}else if(st.equals("s")){
			tmp = this.listeMob.get(i).collision(a.getX(),a.getY()+1);
		}else if(st.equals("q")){
			tmp = this.listeMob.get(i).collision(a.getX()-1,a.getY());
		}else if(st.equals("d")){
			tmp = this.listeMob.get(i).collision(a.getX()+1,a.getY());
		}
		}
		return tmp;
	}

	public Labyrinthe getLabyrinthe() {
		return l;
	}

	public Aventurier getAventurier() {
		return a;
	}
	public ArrayList<Monstre> getListeMob() {
		return listeMob;
	}

	@Override
	public void evoluer(Commande c) {
		this.a.setAtt();
		this.a.initHit();
		if (c.bas) {
			this.actionPersonnage ("s");
		}
		if (c.haut) {
			this.actionPersonnage("z");
		}
		if (c.gauche) {
			this.actionPersonnage("q");
		}
		if (c.droite) {
			this.actionPersonnage("d");
		}
		if (c.attaque) {
			this.actionPersonnage("a");
			for(int i=0;i<this.listeMob.size();i++) {
				if(this.listeMob.get(i).estMort()) {
					this.listeMob.remove(i);
				}
			}
		}
		Cases tab[][]=l.getTabcase();
		for(int i=0;i<tab.length;i++) {
			for(int j=0;j<tab.length;j++) {
			if(tab[i][j] instanceof CasePiege) {
			verifPiege((CasePiege) tab[i][j],a.getCases());
			}
			}
		}
		if (c.ramasser) {
			this.actionPersonnage("e");
		}
		
	}
	
	public void aventurierSurSortie() {
		int x = this.a.getX();
		int y = this.a.getY();
		if(this.l.etreCaseSortie(x,y)) {
			this.l = new Labyrinthe(2);
		}
	}
	public void verifPiege(CasePiege c1,Cases c2){
		if(c1.getX()==c2.getX() && c1.getY()==c2.getY()) {
			a.subirDegat(c1.getDegat());
		}
	}

	@Override
	public boolean etreFini() {
		return fini;
	}

}

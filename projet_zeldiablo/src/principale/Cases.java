package principale;

import java.awt.image.BufferedImage;

public class Cases {
	protected BufferedImage img;
	protected boolean obstacle;
	private int x;
	private int y;
	private boolean contient;


	public Cases(int dx, int dy){
		this.x=dx;
		this.y=dy;
		contient = false;
		obstacle = false;
	}


	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setObstacle(boolean obs) {
		this.obstacle = obs;
	}
	
	public boolean estObstacle() {
		return obstacle;
	}
	public boolean aCote(Cases c) {
		boolean acote=false;
		//case en dessous
		if(this.x==c.getX() && this.y==c.getY()-1) {
			acote=true;
		}
		//case au dessus
		if(this.x==c.getX() && this.y==c.getY()+1) {
			acote=true;
		}
		//case droite
		if(this.x==c.getX()-1 && this.y==c.getY()) {
			acote=true;
		}
		//case 	gauche
		if(this.x==c.getX()+1 && this.y==c.getY()) {
			acote=true;
		}
		return acote;
	}
	
	public boolean isContient() {
		return contient;
	}
	public BufferedImage getImage() {
		return this.img;
	}

}

package principale;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Monstre extends Personnage {


	public Monstre(int dx, int dy) {
		this.ca=new Cases(dx,dy);
		this.pdv=3;
		if(img==null) {
			try {
				img = ImageIO.read(new File("src\\image\\monstre.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		if(imghit==null) {
			try {
				imghit = ImageIO.read(new File("src\\image\\monstrehit.png"));
				imghit=imghit.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
	}
	

	public String genererDirection() {
		String[]tab= {"z", "q", "s", "d"};
		int ind = (int)(Math.random()*4);
		return tab[ind];
	}

	public boolean estMort() {
		if(this.pdv==0) {
			return true;
		}else {
			return false;
		}
	}

}

package principale;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;



public class DessinPerso implements moteurJeu.DessinJeu{

	private static int TAILLE=50;
	private Jeu pers;
	private Labyrinthe l;
	
	public DessinPerso(Jeu j, Labyrinthe l) {
		this.pers = j;
		this.l = l;
	}

	
	@Override
	public void dessiner(BufferedImage image) {
		Graphics2D g = (Graphics2D)image.getGraphics();
		g.setColor(Color.BLACK);
		Cases[][] tab = l.getTabcase();
		for (int i = 0; i< tab.length; i++) {
			for (int j = 0; j <tab.length; j++) {
				if (tab[i][j] instanceof CaseVide || tab[i][j] instanceof CasePiege) {
					g.drawImage(tab[i][j].getImage(),tab[i][j].getX()*TAILLE,tab[i][j].getY()*TAILLE,TAILLE ,TAILLE,null);	
				}
				
				if (tab[i][j] instanceof CaseMur) {
					g.drawImage(tab[i][j].getImage(),tab[i][j].getX()*TAILLE,tab[i][j].getY()*TAILLE,TAILLE ,TAILLE,null);
				}
				if(tab[i][j] instanceof CaseObjet) {
					g.setColor(Color.YELLOW);
					g.fillRect(tab[i][j].getX()*TAILLE, tab[i][j].getY()*TAILLE, TAILLE, TAILLE);
				}
				if(tab[i][j] instanceof CaseSortie) {
					g.drawImage(tab[i][j].getImage(),tab[i][j].getX()*TAILLE,tab[i][j].getY()*TAILLE,TAILLE ,TAILLE,null);
				}
				if(tab[i][j] instanceof CaseObjet) {
					g.drawImage(tab[i][j].getImage(),tab[i][j].getX()*TAILLE,tab[i][j].getY()*TAILLE,TAILLE ,TAILLE,null);
				}
			}
		}
		
		for(int i=0;i<pers.getListeMob().size();i++) {
		
			if(pers.getListeMob().get(i).getHit()) {
				g.drawImage(pers.getListeMob().get(i).getImageHit(),pers.getListeMob().get(i).getX()*TAILLE,pers.getListeMob().get(i).getY()*TAILLE,TAILLE ,TAILLE,null);
			}
			if(!pers.getListeMob().get(i).getHit()) {
				g.drawImage(pers.getListeMob().get(i).getImage(),pers.getListeMob().get(i).getX()*TAILLE,pers.getListeMob().get(i).getY()*TAILLE,TAILLE ,TAILLE,null);
			}
		}
		
		
		Aventurier a = pers.getAventurier();
		if(a.getAtt()) {
			g.drawImage(a.getImageAtt(),a.getX()*TAILLE,a.getY()*TAILLE,TAILLE ,TAILLE,null);
		}
		if(a.getHit()) {
			g.drawImage(a.getImageHit(),a.getX()*TAILLE,a.getY()*TAILLE,TAILLE ,TAILLE,null);
		}
		if(a.getAtt()==false && a.getHit()==false) {
		g.drawImage(a.getImage(),a.getX()*TAILLE,a.getY()*TAILLE,TAILLE ,TAILLE,null);
		}
		
		if (pers.etreFini()){
			g.setColor(Color.red);
			g.setFont(new Font( "Aial", 50,50));
			g.drawString("GAME OVER !!!!", 3*TAILLE+20, 7*TAILLE);
		}
	
}
}
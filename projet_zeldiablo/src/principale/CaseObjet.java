package principale;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class CaseObjet extends Cases{
	
	private Objet o;
	
	public Objet getObjet() {
		return o;
	}

	public void setObjet(Objet o) {
		this.o = o;
	}

	public CaseObjet(int dx, int dy, Objet objet) {
		super(dx, dy);
		o = objet;
		if(img==null) {
			try {
				img = ImageIO.read(new File("src/image/caseobjet.png"));
				img=img.getSubimage(0 , 0, 32, 32);
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
		
	}
	}
	

}

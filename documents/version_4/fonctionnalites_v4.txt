* Fonctionnalités version 4


- fonctionnalité  7.1 : Inventaire
- descriptif texte : Lorsque le joueur appuie sur la touche (e) , il recupere l'objet sur sa case et le met dans l'inventaire
- critères de validation :	
		- L'aventurier recupere l'objet lorsqu'on appuie sur la touche
		- Des objets sont placé dans le labyrinthe
		- Si l'aventurier decide de ramasser, il ne peut pas bouger ni attaquer
-Tests: Verifier qu'il y a des objets
		Verifier qu'il peut les ramasser
		Verifier qu'il les places dans l'inventaire
		 

fonctionnalité : Interface graphique
- descriptif texte : Ajout d'un interface graphique 
- critères de validation :	
		- Les monstres et l'aventurier sont representés
		- Le labyrinthe composé de mur est representé
		- Les objets au sol sont representés 
-Tests: Tests à l'affichage
		